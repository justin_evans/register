##############################
# Makefile
##############################
CC		= gcc
CFLAGS		= -g -Wall -I.
DEPS		= register.h
SOURCES		= ./src/
BIN		= ./bin/
OBJ		= register
INSTALL		= /usr/local/bin/
##############################
all: register

register:
	mkdir -p bin && $(CC) -o $(BIN)$(OBJ) $(SOURCES)*.c $(CFLAGS)

test:
	valgrind --leak-check=yes $(BIN)$(OBJ)

install:
	cp -v bin/$(OBJ) $(INSTALL)$(OBJ)

uninstall:
	rm -fv $(INSTALL)$(OBJ)

clean:
	rm -rfv bin
#############################

