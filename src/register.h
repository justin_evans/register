///////////////////////////
// register.h            //
// REGISTRATION HEADER   //
// Interface             //
// Justin R. Evans       //
///////////////////////////

// For Crypt
#ifndef SALT_LEN
	#define SALT_LEN 8
#else
	#undef SALT_LEN
	#define SALT_LEN 8
#endif

// Debugging Flag
#ifndef DEBUG
	#define DEBUG 0
#else
	#undef DEBUG
	#define DEBUG 0
#endif

// Detect Linux OR Mac OS X OR Unix
// Load appropriate "crypt" headers

#ifdef __APPLE__
	#include <mcrypt.h>
#elif __linux__
	#include <crypt.h>
#elif __unix__
	#include <crypt.h>
#endif

// Messages

static const char* ACCOUNT_CREATED = "Account successfully created!";
static const char* PASSWORD_MISMATCH = "Passwords do not match!";

// Structures

struct user {
	char username[50];
	char password[20];
	int validated;
};

typedef struct user User;

// Prototypes
extern const char* get_username();
extern char* generate_password_hash();
extern int verify_password(char pass[20]);

