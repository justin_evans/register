/////////////////////////
// register.c          //
// REGISTRATION MODULE //
// Implementation      //
// Justin R. Evans     //
/////////////////////////

// Dependencies

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

// Interface

#include "register.h"

// Implementation

int main(int argc, char *argv[])
{
	User _user;
	_user.validated = 0;

	strcpy(_user.username, get_username());

	strcpy(_user.password, generate_password_hash());

	_user.validated = verify_password(_user.password);

	if(_user.validated == 0) {
		printf("\n%s\n\n", PASSWORD_MISMATCH);
		return 1;
	} else {
		printf("\n%s\n\n", ACCOUNT_CREATED);
	}

	return 0;
}

const char *get_username()
{
	char *username;

   	username = (char *) malloc(50);

	printf("\nEnter username: ");
	scanf("%s", &username[0]);

	return username;
}

char *generate_password_hash()
{
	unsigned long seed[2];
	char salt[] = "$1$........";
	const char *const seedchars =
		"./0123456789"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz";

	int i;

	seed[0] = time(NULL);
	seed[1] = getpid() ^ (seed[0] >> 14 & 0x30000);

	for(i=0; i < SALT_LEN; ++i)
	{
		salt[3 + i] = seedchars[(seed[i / 5] >> (i % 5) * 6) & 0x3f];
	}

	char *password = crypt(getpass("Enter password:"), salt);

	return password;
}

int verify_password(char pass[20])
{
	char result[20];
	int ok = 0;
	char *confirm_password = crypt(getpass("Re-enter password:"), pass);

	strcpy(result, confirm_password);

	ok = strcmp(result, pass) == 0;

	return ok;
}

