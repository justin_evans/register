# Compile with Debugging output
# Build script for Linux (requires "crypt.h", and -lcrypt link flag):
# `gcc -g registration_test.c -lcrypt -o bin/register`

# Build script for Mac OS X (requires "mcrypt.h")
`gcc -g ../src/registration_test.c -lcrypt -o ../bin/register_test`

