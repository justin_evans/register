REGISTRATION MODULE
===================

Simple module written in `C` to register/encrypt password and username combination.

***Install:***

`./build.sh` 

***Run:***

`./bin/register`

See `docs` for more information.

## Dependencies

- [Valgrind](http://valgrind.org/) - Instrumentation Framework
- [Doxygen](http://www.stack.nl/~dimitri/doxygen/) - Documentation
